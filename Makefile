
# Determine the target system.
TARGET = $(shell uname -sm | sed 's/ /-/g')
SHORTTARGET = $(shell uname -sm | cut -d"_" -f1)
BUILDOUTPUT  = ./binaries/$(TARGET)

# List of include directories (no '-I' prefix).
INC_DIRS = ./include ./headers
DEFINES = 

# Flags passed to the preprocessor.
# Set Google Test's header directory as a system directory, such that
# the compiler doesn't generate warnings in Google Test headers.
CPPFLAGS += 

# Flags passed to the C++ compiler.
CXXFLAGS +=  $(addprefix -D,$(DEFINES)) \
             $(addprefix -I,$(INC_DIRS)) \
             -Wno-unknown-pragmas \
             -Wall \
             -Wextra \
             -Wconversion \
             -Wformat=2 \
             -Wstack-protector \
			 -Wmissing-declarations \
			 -std=c++0x

# Any additional libraries required
LIBRARIES +=

ifeq ("$(BUILDTYPE)", "")
  BUILDTYPE=debug
endif

ifeq ("$(BUILDTYPE)", "debug")
  CPPFLAGS +=
  CXXFLAGS += -O0 -g \
              -coverage  \
              -fstack-protector-all \
              -fno-common \
              -fno-omit-frame-pointer
  LIBRARIES += 
endif

ifeq ("$(BUILDTYPE)", "release")
  CPPFLAGS +=
  CXXFLAGS += -O2  \
              -fstack-protector-all \
              -fno-common \
              -fno-omit-frame-pointer
  LIBRARIES +=
endif

# Command to make an object file:
COMPILE = $(CXX) $(CXXFLAGS) -c

MKDEPENDS = $(CXX) $(CPPFLAGS)  \
			$(addprefix -D,$(DEFINES)) \
            $(addprefix -I,$(INC_DIRS)) \
			-std=c++0x -MM

# Command used to link objects:
# LD is sort of traditional as well,
# probably stands for Linker-loaDer
LD = $(CXX)

# All tests produced by this Makefile.  Remember to add new tests you
# created to the list.
APPLICATION = Example 

# House-keeping build targets.
OBJECTS = $(BUILDOUTPUT)/main.o \
          $(BUILDOUTPUT)/Example.o 
DEPENDFILES = $(OBJECTS:.o=.depends)

$(APPLICATION) : $(OBJECTS) $(BUILDOUTPUT)/main.o
	$(LD) $(CPPFLAGS) $(CXXFLAGS) $(LIBRARIES) $^ -o $(BUILDOUTPUT)/$@

all : $(APPLICATION)
	@echo "Build Complete"

clean :
	rm -f $(BUILDOUTPUT)/$(APPLICATION)
	rm -f $(BUILDOUTPUT)/*.o
	rm -f $(DEPENDFILES)
	rm -f $(BUILDOUTPUT)/*.depends

world :
	clean all

.PHONY :
	all clean world

# Canned commands

define DO_COMPILE
mkdir -p $(@D)
$(COMPILE) $< -o $@
endef

define DO_DEPENDS
mkdir -p $(@D)
echo "$@ \\" > $@
$(MKDEPENDS) -MT $(@:.depends=.o) $< >> $@
endef

# Build rules/associations
$(BUILDOUTPUT)/%.o : ./source/%.cpp
	$(DO_COMPILE)
	
# Dependancy tracking
$(BUILDOUTPUT)/%.depends : ./source/%.cpp
	$(DO_DEPENDS)

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPENDFILES)
endif

#!EOF


